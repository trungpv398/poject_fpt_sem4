package tomato_th.project_tomato.service.Impl.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import tomato_th.project_tomato.model.core.Orders;
import tomato_th.project_tomato.repository.core.OrderRepository;
import tomato_th.project_tomato.service.OrderService;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Override
    public List<Orders> getAllOrders() {
        try{
            List<Orders> list = orderRepository.findAll();
            return list;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Orders getOrderById(int id) {
        try{
            Orders orders = orderRepository.findById(id).get();
            return orders;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean updateOrderStatus(int id,int status) {
        try{
            Orders orders = orderRepository.findById(id).get();
            orders.setUpdated(new Date());
            orders.setStatus(status);
            orderRepository.save(orders);
            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Orders saveOrder(Orders orders) {
        try{
            orders.setStatus(1);
            orders.setUpdated(new Date());
            orders.setCreated(new Date());
            Orders orderNew = orderRepository.save(orders);
            return orderNew;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean canceledOrder(int order_id,int status) {
        try{
            if(status==1)
            {
                Orders orderUpdate = orderRepository.findById(order_id).get();
                orderUpdate.setStatus(5);
                orderRepository.save(orderUpdate);
                return true;
            }
            return false;

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int countOrderByStatus(int status) {
        List<Orders> list = orderRepository.findAllByStatus(status);
        int count = list.size();
        return count;
    }

    @Override
    public List<Orders> listOrderMonth(int month) {
        try{
            List<Orders> listMonth = orderRepository.getOrderComplete(month);
            return listMonth;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public float totalByMonth(String startDate, String endDate) {
        try{
            float b =0;
            b = orderRepository.getOrder1Complete(new SimpleDateFormat("yyyy-MM-dd").parse(startDate),new SimpleDateFormat("yyyy-MM-dd").parse(endDate));
            return b;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Page<Orders> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return this.orderRepository.findPaginateOrder(pageable);
    }

    @Override
    public Page<Orders> findPagiCustomer(int pageNo, int pageSize, int cust_id) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return this.orderRepository.getByCustomerIdPaginate(cust_id,pageable);
    }

    @Override
    public Page<Orders> findPagiWatting(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return this.orderRepository.findPagiOrderWatting(pageable);
    }

    @Override
    public Page<Orders> findPagiConfirmed(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return this.orderRepository.findPagiOrderConfirmed(pageable);
    }

    @Override
    public Page<Orders> findPagiShipping(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return this.orderRepository.findPagiOrderShipping(pageable);
    }

    @Override
    public Page<Orders> findPagiComplete(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return this.orderRepository.findPagiOrderComplete(pageable);
    }

    @Override
    public Page<Orders> findPagiCancelled(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return this.orderRepository.findPagiOrderCancelled(pageable);
    }
}
