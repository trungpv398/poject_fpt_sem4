package tomato_th.project_tomato.service;

import org.springframework.data.domain.Page;
import tomato_th.project_tomato.model.core.Orders;


import java.util.List;

public interface OrderService {
    List<Orders> getAllOrders();
    Orders getOrderById(int id);
    boolean updateOrderStatus(int id, int status);
    Orders saveOrder(Orders orders);
    boolean canceledOrder(int order_id, int status);
    int countOrderByStatus(int status);

    List<Orders> listOrderMonth(int month);

    float totalByMonth(String startDate, String endDate);

    Page<Orders> findPaginated(int pageNo, int pageSize);

    Page<Orders> findPagiCustomer(int pageNo, int pageSize, int cust_id);

    Page<Orders> findPagiWatting(int pageNo, int pageSize);
    Page<Orders> findPagiConfirmed(int pageNo, int pageSize);
    Page<Orders> findPagiShipping(int pageNo, int pageSize);
    Page<Orders> findPagiComplete(int pageNo, int pageSize);
    Page<Orders> findPagiCancelled(int pageNo, int pageSize);

}
