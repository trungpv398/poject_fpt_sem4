package tomato_th.project_tomato.repository.core;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tomato_th.project_tomato.model.core.Orders;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Orders,Integer> {
    @Query("SELECT c FROM Orders c WHERE  customer_id = ?1")
    List<Orders> getByCustomerId(int customerId);

    @Query("SELECT c FROM Orders c WHERE  customer_id = ?1")
    Page<Orders> getByCustomerIdPaginate(int customerId, Pageable pageable);

    List<Orders> findAllByStatus(int status);

    @Query("SELECT o FROM Orders o ORDER BY status ASC ")
    Page<Orders> findPaginateOrder(Pageable pageable);

    @Query("SELECT o FROM Orders o WHERE status = 1 ORDER BY created ASC ")
    Page<Orders> findPagiOrderWatting(Pageable pageable);

    @Query("SELECT o FROM Orders o WHERE status = 2 ORDER BY updated ASC ")
    Page<Orders> findPagiOrderConfirmed(Pageable pageable);

    @Query("SELECT o FROM Orders o WHERE status = 3 ORDER BY updated ASC ")
    Page<Orders> findPagiOrderShipping(Pageable pageable);

    @Query("SELECT o FROM Orders o WHERE status = 4 ORDER BY updated ASC ")
    Page<Orders> findPagiOrderComplete(Pageable pageable);

    @Query("SELECT o FROM Orders o WHERE status = 5 ORDER BY updated ASC ")
    Page<Orders> findPagiOrderCancelled(Pageable pageable);

    @Query(value = "from Orders WHERE month(created)= ?1",nativeQuery = true)
    List<Orders> getOrderComplete(int month);

    @Query(value = "SELECT SUM(o.total_price) from Orders o where status = 4 AND created BETWEEN :startDate AND :endDate")
    float getOrder1Complete(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

//    @Query(value = "SELECT o FROM Orders o WHERE EXTRACT (month FROM o.created) =: ?1 AND status = 4", nativeQuery = true)
//    List<Orders> getOrderComplete(int month);
}
