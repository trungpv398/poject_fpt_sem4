package tomato_th.project_tomato.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tomato_th.project_tomato.model.ClImage;
import tomato_th.project_tomato.model.News;
import tomato_th.project_tomato.model.TypeNews;
import tomato_th.project_tomato.service.CdService;
import tomato_th.project_tomato.service.NewsService;
import tomato_th.project_tomato.service.TypeNewsService;

import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/admin/news")
public class NewsController {
    @Autowired
    private NewsService newsService;
    @Autowired
    private TypeNewsService typeNewsService;
    @Autowired
    private CdService cdService;

    @InitBinder
    public void InitBinder(WebDataBinder data)
    {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        data.registerCustomEditor(Date.class, new CustomDateEditor(s, true));
    }

    @RequestMapping("")
    public String getNews(Model model)
    {
        return findPaginated(1,model);
    }

    @RequestMapping(path = "/insertNews")
    public String insertNews(Model model)
    {
        News news = new News();
        List<TypeNews> listTypeNew = typeNewsService.listByStatusShow();
        model.addAttribute("newsNew",news);
        model.addAttribute("listType",listTypeNew);

        return "admin/news/insertNews";
    }

    @RequestMapping(path = "/saveNews",method = RequestMethod.POST)
    public String saveNews(@ModelAttribute("newsNew")@Valid News news, BindingResult bindingResult, Model model, @RequestParam("file_avatar") MultipartFile multipartFile)throws IOException
    {
        if(bindingResult.hasErrors())
        {
            List<TypeNews> listTypeNew = typeNewsService.listByStatusShow();
            model.addAttribute("newsNew",news);
            model.addAttribute("listType",listTypeNew);

            return "admin/news/insertNews";
        }
        if (multipartFile.getSize()==0)
        {
            return "redirect:/admin/news?error=Image link must not be empty";
        }

        Map resultCd = cdService.upload(multipartFile);
        ClImage clImage = new ClImage((String)resultCd.get("secure_url"));
        news.setImageLink(clImage.getUrl());
        boolean bl = newsService.saveNew(news);
        if (bl)
        {
            return "redirect:/admin/news?success=Add News success";
        }
        return "redirect:/admin/news?error=Add News error";
    }

    @RequestMapping(value = "/detailNews")
    public String detailNew(@RequestParam("id")Integer id, Model model)
    {
        News news = newsService.getNewById(id);
        model.addAttribute("newDetail",news);
        return "admin/news/detailNews";
    }

    @RequestMapping(value = "/editNews")
    public String editNew(@RequestParam("id")Integer id, Model model)
    {
        News news = newsService.getNewById(id);
        List<TypeNews> listTypeNew = typeNewsService.listByStatusShow();
        model.addAttribute("listType",listTypeNew);
        model.addAttribute("newEdit",news);
        return "admin/news/editNews";
    }

    @RequestMapping(value = "/updateNews",method = RequestMethod.POST)
    public String updateNew(@ModelAttribute("newEdit")News news, @RequestParam("file_avatar") MultipartFile multipartFile)throws IOException
    {
        News newEdit = newsService.getNewById(news.getNews_id());
        if (multipartFile.getSize()>0)
        {
            Map resultCd = cdService.upload(multipartFile);
            ClImage clImage = new ClImage((String)resultCd.get("secure_url"));
            news.setImageLink(clImage.getUrl());
        }else{
            news.setImageLink(newEdit.getImageLink());
        }
        boolean bl = newsService.updateNew(news);
        if (bl)
        {
            return "redirect:/admin/news?success=Update News success";
        }
        return "redirect:/admin/news?error=Update News error";
    }
    @RequestMapping(value = "/deleteNews")
    public String deleteNew(@RequestParam("id")Integer id)
    {
        boolean bl = newsService.deleteNew(id);
        if (bl)
        {
            return "redirect:/admin/news?success=Delete News success";
        }
        return "redirect:/admin/news?error=Delete News error";
    }

    @RequestMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo")int pageNo, Model model)
    {
        int pageSize = 10;
        Page<News> page = newsService.findPaginated(pageNo,pageSize);
        List<News> listNews = page.getContent();
        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",page.getTotalPages());
        model.addAttribute("totalItems",page.getTotalElements());
        model.addAttribute("list",listNews);
        return "admin/news/newsList";
    }

}
