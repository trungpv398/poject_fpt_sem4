package tomato_th.project_tomato.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import tomato_th.project_tomato.service.OrderService;

import java.text.SimpleDateFormat;
import java.util.Date;


@Controller
@RequestMapping(path = "/admin")
public class WebController {
    @Autowired
    private OrderService orderService;

    @InitBinder
    public void InitBinder(WebDataBinder data) {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        data.registerCustomEditor(Date.class, new CustomDateEditor(s, true));
    }

    @RequestMapping
    public String index(Model model)
    {
        //chartRevenue(model);

        return "admin/home";
    }


    public void chartRevenue(Model model)
    {
        float january = orderService.totalByMonth("2021-01-01" ,"2021-01-31");
        model.addAttribute("january",january);
        float february = orderService.totalByMonth("2021-02-01" ,"2021-02-31");
        model.addAttribute("february",february);
        float march = orderService.totalByMonth("2021-03-01" ,"2021-03-31");
        model.addAttribute("march",march);
        float april = orderService.totalByMonth("2021-04-01" ,"2021-04-31");
        model.addAttribute("april",april);
        float may = orderService.totalByMonth("2021-05-01" ,"2021-05-31");
        model.addAttribute("may",may);
        float june = orderService.totalByMonth("2021-06-01" ,"2021-06-31");
        model.addAttribute("june",june);
        float july = orderService.totalByMonth("2021-07-01" ,"2021-07-31");
        model.addAttribute("july",july);
        float august = orderService.totalByMonth("2021-08-01" ,"2021-08-31");
        model.addAttribute("august",august);
        float september = orderService.totalByMonth("2021-09-01" ,"2021-09-31");
        model.addAttribute("september",september);
        float october = orderService.totalByMonth("2021-10-01" ,"2021-10-31");
        model.addAttribute("october",october);
        float november = orderService.totalByMonth("2021-11-01" ,"2021-11-31");
        model.addAttribute("november",november);
        float december = orderService.totalByMonth("2021-12-01" ,"2021-12-31");
        model.addAttribute("december",december);
    }

}
